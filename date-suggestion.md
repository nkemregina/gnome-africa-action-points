# GNOME Africa Monthly Meeting Timing Survey

Thank you for your participation in helping us find the best time to host our monthly meetings! Your input is valuable in ensuring that our meetings are accessible to as many community members as possible.

**Instructions:** Please take a few moments, 2 mins at most to answer the following questions. Your responses will guide us in scheduling the meetings effectively.

## Participant Information

- **Name:** [Your Name]
- **Email:** [Your Email]
- **Country/Region:** [Your Country/Region]

## Meeting Frequency

1. How often would you prefer to have the GNOME Africa meetings?
   - [ ] Once a month
   - [ ] Twice a month
   - [ ] Other (please specify): [ ]

## Preferred Day

2. Which day(s) of the week are you most available for attending meetings? (Select all that apply)
   - [ ] Monday
   - [ ] Tuesday
   - [ ] Wednesday
   - [ ] Thursday
   - [ ] Friday
   - [ ] Saturday
   - [ ] Sunday

## Meeting Time

3. What time(s) of the day are you most comfortable with for attending meetings? (Please indicate your local time)
   - [ ] Morning (9:00 AM - 10:00 AM)
   - [ ] Afternoon (12:00 PM - 2:00 PM)
   - [ ] Evening (4:00 PM - 6:00 PM)
   - [ ] Night (7:00 PM - 9:00 PM)
   - [ ] Other (please specify): [ ]


## Additional Comments

4. Do you have any additional comments or suggestions regarding the meeting schedule? Please feel free to share your thoughts here:

[Your Comments]

## Thank You

Thank you for taking the time to complete this survey. Your input will help us determine the most suitable time for our monthly meetings. We look forward to your continued participation in the GNOME Africa community!


*See Form Output here To submit your responses, please use the following link: [GNOME Africa Meeting Timing Survey](https://forms.gle/d5MnnTP3iec6gfkE9)*

