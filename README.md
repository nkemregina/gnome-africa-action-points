# How to Contribute to GNOME Africa Community

Welcome to the GNOME Africa community! We're excited to have you here. Below are some guidelines on how to contribute to our community and use the issue tracker effectively:

## How to Contribute to GNOME Africa Community

1. **Participate in Meetings:** Attend our regular meetings to stay updated on community activities and share your ideas.
2. **Join Discussions:** Engage in discussions on our GitLab repository and mailing list to contribute your insights and perspectives.
3. **Contribute Code:** Help us improve GNOME Africa by contributing code, documentation, or translations.
4. **Spread the Word:** Promote GNOME Africa on social media and among your networks to help us grow our community.
5. **Share Resources:** Share useful resources, such as articles, tutorials, or tools, that can benefit our community members.
6. **Provide Feedback:** Provide constructive feedback and suggestions to help us improve our processes and initiatives.

## Guidelines for Creating Issues

When creating issues on our GitLab repository, please follow these guidelines:

1. **Include Meeting Minutes:** If relevant, include meeting minutes in the issue description, specifying the date of the meeting to provide a traceable record.
2. **Assign an Assignee:** Every issue should have an assignee to take ownership and responsibility for addressing it.
3. **Use Task Lists:** Use task lists to assign sub-tasks to individuals and track progress effectively.
4. **Tag Users:** Whenever necessary, tag users using their GitLab usernames  to notify them of the issue and facilitate communication.
5. **Use Appropriate Labels:** Apply appropriate labels to describe the nature of the issue:

   - **Technical Initiative:** Use this label for technical initiatives aimed at helping users understand how to use GNOME technologies or contribute to the GNOME project.
   - **Documentation:** Use this label for tasks related to documentation efforts within the GNOME project, including creating, updating, and maintaining documentation resources.
   - **Socials:** Use this label for tasks related to managing and promoting GNOME's presence on social media platforms, such as creating and scheduling posts, engaging with followers, and analyzing social media performance metrics.
   - **Stars of the Month:** Use this label to recognize outstanding contributors or community members within the GNOME Africa Community for their exceptional contributions, leadership, or positive impact during a specific month.
   - **Twitter Space:** Use this label for issues related to organizing or participating in Twitter Spaces hosted by the GNOME Africa Twitter handle.
   - **Bug:** Use this label for issues related to identifying and fixing bugs in the GNOME Africa project.
   - **Feature Request:** Use this label for suggesting new features or enhancements to the GNOME Africa project.
   - **In Progress:** Use this label to indicate that work is currently in progress on the issue.
   - **Help Wanted:** Use this label to indicate that additional help or expertise is needed to address the issue.

## People to Tag When Creating an Issue

Always tag @regina_nkenchor, @maryannonuoha901, @ayooluwa.olosunde, @Nasah-Kuma, @FIBI, @iamChekwube, @dvdnwoke, for quick responses. Please note that GNOME Africa is a volunteer community, so we kindly ask for your patience.

## Additional Resources

For more information about GNOME Africa and our ongoing initiatives, please refer to our [wiki](https://wiki.gnome.org/GnomeAfrica) and [meeting notes](https://gitlab.gnome.org/Teams/Engagement/gnome-africa/gnomeafrica/-/wikis/home).

You can also check out our Linktree account for additional resources: [Linktree](https://linktr.ee/gnome_africa_community)

Thank you for your contributions and patience as we work together to grow and improve the GNOME Africa community! 🌍💻


